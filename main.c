#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//Maximos
#define PACIENTE_MAX 10
#define CONSULTA_MAX 10
#define MEDICO_MAX 10


//Estruturas 
struct medico {
    char nome[50];
    char especialidade[50];
    int crm;
};

struct filaMedico {
    struct medico medico[MEDICO_MAX];
    int fim;
};

struct paciente {
    int id;
    char nome[50];
    char endereco[50];
    int plano;
    };

struct consulta {
    int id;
    int dataConsulta;
    int dia;
    int mes;
    int ano;
    int crmMedico;
    int idPaciente;
    char problema[100]; 
};

struct filaConsulta {
    struct consulta consulta[CONSULTA_MAX];
    int fim;
};
// Funcao de exibir menu
void menu()
{
    int choice, c = 0;
    while(choice != 10)
    {
        //System("cls");
        printf(" ---------- Hospital Gregory ----------- \n\n");
        printf(" ---------- Menu ----------- \n");
        printf("Escolha uma opção: \n");
        printf("1 - Paciente \n");
        printf("2 - Medico \n");
        printf("3 - Consulta \n");
        scanf("%d", &choice);
        
        switch(choice)
        {
            case 1: //paciente
            //System("cls");
            printf(" ---------- Menu ----------- \n");
            printf("Escolha uma opção: \n");
            printf("1 - Cadastrar paciente \n");
            printf("2 - Pesquisar paciente \n");
            printf("3 - Alterar paciente \n");
            printf("4 - Excluir paciente \n");
            scanf("%d", &c);
            switch(c)
            {
                case 1:
                cadastrarPaciente();
                break;
                
                case 2:
                pesquisarPaciente();
                break;
                
                case 3:
                alterarPaciente();
                break;
                
                case 4:
                excluirPaciente();
                break;
            }
            break;
            
            case 2: //medico
            //System("cls");
            printf(" ---------- Menu ----------- \n");
            printf("Escolha uma opção: \n");
            printf("1 - Cadastrar médico \n");
            printf("2 - Pesquisar médico \n");
            printf("3 - Alterar médico \n");
            printf("4 - Excluir médico \n");
            scanf("%d", &c);
            switch(c)
            {
                case 1:
                cadastrarMedico();
                break;
                
                case 2:
                pesquisarMedico();
                break;
                
                case 3:
                alterarMedico();
                break;
                
                case 4:
                excluirMedico();
                break;
            }
            break;
            
            case 3: //consulta
                //System("cls");
                printf(" ---------- Menu ----------- \n");
                printf("Escolha uma opção: \n");
                printf("1 - Cadastrar consulta \n");
                printf("2 - Pesquisar consulta \n");
                printf("3 - Listar consultas por médico \n");
                printf("4 - Alterar médico \n");
                printf("5 - Excluir médico \n");
                scanf("%d", &c);
                
                switch(c)
                {
                    case 1:
                    cadastrarConsulta();
                    break;
                    
                    case 2:
                    pesquisarConsulta();
                    break;
                    
                    case 3:
                    listaConsultaPorMedico();
                    break;
                    
                    case 4:
                    alterarConsulta();
                    break;
                    
                    case 5:
                    excluirConsulta();
                    break;
                }
            break;
            
            case 0:
            printf("Saindo do sistema\n");
            printf("Se você fala com Deus, você é religioso. Se Deus fala com você, você é psicótico. - Gregory House\n");
            return 0;
            break;
            
            default:
            printf("Você digitou um valor incorreto\n");
            printf("\"O seu raciocínio não presta. Para a próxima, use o meu! - Gregory House.\"");
            break;
        }
        
    }
}

// Arrays Globais
struct paciente pacientes[PACIENTE_MAX];
struct filaMedico medicos;
struct filaConsulta consultas;

const char *tipoPlano[4]={"", "Básico", "Empresarial", "Premium"};
int countPaciente = 0;
int countConsulta = 0;

//Funcao de pausar
void pause()
{
    char qualquer_tecla[10];
    printf("Digite V para voltar ao menu: ");
    scanf(" %s", qualquer_tecla);
    return 0;
}

//Funcoes Paciente
void cadastrarPaciente()
{
    if(countPaciente < PACIENTE_MAX)
    {
        pacientes[countPaciente].id = countPaciente;
        printf("Nome do paciente:  ");
        scanf(" %s", pacientes[countPaciente].nome);
        
        printf("Endereço:  ");
        scanf(" %s", pacientes[countPaciente].endereco);
       
        int testePlano = 0;        
        pacientes[countPaciente].plano = testePlano;
        
        do 
        {            
            printf("Plano (Escolha uma opção):  \n");
            printf("1 - Básico\n");
            printf("2 - Empresarial\n");
            printf("3 - Premium\n");
            printf("Plano: ");
            scanf(" %d", &testePlano);
            if (testePlano < 1 || testePlano > 3)
            {
                printf("Plano inválido. Por favor preencha um valor válido\n");
            }
        } while (testePlano < 1 || testePlano > 3);
        
        printf("Paciente %s cadastrado com sucesso\n", pacientes[countPaciente].nome);
        countPaciente++;
        pause();
    }
    else
    {
        printf("Máximo de clientes alcançado. Vá curar os outros primeiro.\n");
        pause();
    }
}
void pesquisarPaciente()
{
    if(countPaciente > 0)
    {
        printf("------ Pesquisa de paciente ------ \n");
        
        char pesquisaPaciente[50];
        printf("Nome do paciente:  ");
        scanf(" %s", pesquisaPaciente);
        int t = 0;
        for (int i = 1; i <= countPaciente; i++)
        {
            if (strcmp(pesquisaPaciente, pacientes[i].nome) == 0)
            {
                printf("Id:                         %d\n", pacientes[i].id);
                printf("Nome do paciente:           %s\n", pacientes[i].nome);
                printf("Endereco paciente:          %s\n", pacientes[i].endereco);
                printf("Plano do paciente:          %s\n", tipoPlano[pacientes[i].plano]);
                t = 1;
                pause(); 
            }
        }
        if (t == 0)
        {
            printf("Paciente não encontrado.\n");
            pause();
        }
    }
    else
    {
        printf("Não há pacientes cadastrados.\n");
        pause();
    }
    
}
void excluirPaciente()
{
    if(countPaciente > 0)
    {
        printf("------ Deletar paciente ------ \n");
        
        char pesquisaPaciente[50];
        printf("Nome do paciente:  ");
        scanf(" %s", pesquisaPaciente);
        int t = 0;
        for (int i = 1; i <= countPaciente; i++)
        {
            if (strcmp(pesquisaPaciente, pacientes[i].nome) == 0)
            {
                pacientes[i].id = 0 ;
                strcpy(pacientes[i].nome, "");
                strcpy(pacientes[i].endereco, "");
                tipoPlano[pacientes[i].plano] = 0;
                t = 1;
                printf("Paciente excluido com sucesso\n");
                pause(); 
            }
        }
        if (t == 0)
        {
            printf("Paciente não encontrado.\n");
            pause();
        }
    }
    else
    {
        printf("Não há pacientes cadastrados.\n");
        pause();
    }
}
void alterarPaciente()
{
    if(countPaciente > 0)
    {
        printf("------ Alterar paciente ------ \n");
        
        char pesquisaPaciente[50];
        printf("Nome do paciente:  ");
        scanf(" %s", pesquisaPaciente);
        int t = 0;
        for (int i = 1; i <= countPaciente; i++)
        {
            if (strcmp(pesquisaPaciente, pacientes[i].nome) == 0)
            {   
                t = 1;
                printf("Nome: %s        Novo nome:  ", pacientes[i].nome);
                scanf(" %s", pacientes[i].nome);
                printf("Endereço: %s    Novo endereço:  ", pacientes[i].endereco);
                scanf(" %s", pacientes[countPaciente].endereco);

                int testePlano = 0;        
                pacientes[i].plano = testePlano;

                do 
                {            
                    printf("Plano (Escolha uma opção):  \n");
                    printf("1 - Básico\n");
                    printf("2 - Empresarial\n");
                    printf("3 - Premium\n");
                    printf("Plano: ");
                    scanf("%d", &testePlano);
                        if (testePlano < 1 || testePlano > 3)
                        {
                            printf("Plano inválido. Por favor preencha um valor válido\n");
                        }
                } while (testePlano < 1 || testePlano > 3);

                printf("Paciente %s alterado com sucesso\n", pacientes[i].nome);
                pause();
            }
        }
        
        if (t == 0)
        {
            printf("Paciente não encontrado.\n");
            pause();
        }
        
    }
    else
    {
        printf("Não há pacientes cadastrados.\n");
        pause();
    }
}
void cadastrarMedico()
{
    if(medicos.fim < MEDICO_MAX)
    {        
        int provs = -5;
        int flag, count = 0;
        printf("---- Cadastrar médico ----\n");
        while (provs < 0)
        {
            printf("CRM:  ");
            scanf("%d", &provs);
        }       
        fflush(stdin);
                        
        //Procura se o CRM já está cadastrado
        for (int i = 0; i < medicos.fim; i++)
        {
            if (provs == medicos.medico[i].crm)
            {                
                flag = 1;
                break;
            }//Se encontrar outro valor igual             
        }
        
        if (flag == 1)
        {
            printf("Médicos já cadastrado\n");
            flag = 0;
            pause();
        }
        else
        {
            //Pega CRM e coloca em ordem na fila
            for(count = 0; count < medicos.fim; count++)
            {
                if (provs < medicos.medico[count].crm)
                {
                    break;
                }
            }
            if(medicos.fim > 0)
            {    //Encontrou o lugar, empurrar os outros para frente
                for (int j = medicos.fim; j > count; j--)
                {
                    medicos.medico[j].crm = medicos.medico[j-1].crm;
                    strcpy(medicos.medico[j].nome, medicos.medico[j-1].nome);
                    strcpy(medicos.medico[j].especialidade,medicos.medico[j-1].especialidade);
                }
            }

            medicos.medico[count].crm = provs;
            printf("Nome do Médico:  ");
            scanf(" %s",  medicos.medico[count].nome);
            printf("Especialidade:  ");
            scanf(" %s",  medicos.medico[count].especialidade);

            printf("Médicos %s cadastrado com sucesso\n", medicos.medico[count].nome);
            medicos.fim++;
            provs = -5; 
            count = 0;           
            pause();
                       
        }
    }    
    else
    {
        printf("Máximo de médicos alcançado.\n");
        pause();
    }
}
void pesquisarMedico()
{
    if(medicos.fim > 0)
    {
        printf("------ Pesquisa de Médico ------ \n");
        
        int pesquisaMedico = -5;        
        while (pesquisaMedico < 0)
        {
            printf("CRM:  ");
            scanf("%d", &pesquisaMedico);
        }       
        fflush(stdin);
        
        int t = 0;
        for (int i = 0; i <= medicos.fim; i++)
        {
            if (pesquisaMedico == medicos.medico[i].crm)
            {
                
                printf("CRM:                    %d\n", medicos.medico[i].crm);
                printf("Nome:                   %s\n", medicos.medico[i].nome);
                printf("Especialidade:          %s\n", medicos.medico[i].especialidade); 
                t = 1;
                pause(); 
                break;
            }  
        }  
       
        if (t == 0)
        {
            printf("Médico não encontrado.\n");
            pause();
        }
    }
    else
    {
        printf("Não há medicos cadastrados.\n");
        pause();
    }
}
void excluirMedico()
{
    if (medicos.fim > 0)
    {
        printf("------ Excluir Médico ------ \n");
        
        int excluiMedico = -5; 
        int count, t = 0;
        while (excluiMedico < 0)
        {
            printf("Digite o CRM:  ");
            scanf("%d", &excluiMedico);
        }       
        fflush(stdin);
        
        // Procurar o medico e excluir
        for (count = 0; count < medicos.fim; count++)
        {
            if (excluiMedico == medicos.medico[count].crm)
            {
                t = 1;
                break;
            }
        }
        
        if (t == 0)
        {
            printf("Médico não encontrado.\n");
            pause();
        } 
        
        //Encontrou o lugar, empurrar os outros para frente
        else if(medicos.fim > 0)
        {  
            for (int j = count; j < medicos.fim; j++)
            {
                medicos.medico[j].crm = medicos.medico[j+1].crm;
                strcpy(medicos.medico[j].nome, medicos.medico[j+1].nome);
                strcpy(medicos.medico[j].especialidade,medicos.medico[j+1].especialidade);
            }
                       
        
        medicos.fim--;        
        printf("Registro exluido com sucesso.\n");
        pause(); 
        } 
    }
    else
    {
        printf("Não há medicos cadastrados.\n");
        pause();
    }
}
void alterarMedico()
{
    if(medicos.fim > 0)
    {
        printf("------ Alterar Médico ------ \n");
        
        int alteraMedico = -5; 
        int i, t = 0;
        while (alteraMedico < 0)
        {
            printf("Digite o CRM:  ");
            scanf("%d", &alteraMedico);
        }       
        fflush(stdin);
        
        
        for ( i = 0; i <= medicos.fim; i++)
        {
            if (alteraMedico == medicos.medico[i].crm)
            {
                
                printf("Nome do Médico:  ");
                scanf(" %s",  medicos.medico[i].nome);
                printf("Especialidade:  ");
                scanf(" %s",  medicos.medico[i].especialidade);

                printf("Médicos %s alterado com sucesso\n", medicos.medico[i].nome);
                printf("CRM:                    %d\n", medicos.medico[i].crm);
                printf("Nome:                   %s\n", medicos.medico[i].nome);
                printf("Especialidade:          %s\n", medicos.medico[i].especialidade);
               
                t = 1;
                pause(); 
            }
        }
                
        if (t == 0)
        {
            printf("Medico não encontrado.\n");
            pause();
        }        
    }
    else
    {
        printf("Não há medicos cadastrados.\n");
        pause();
    }
}
void cadastrarConsulta()
{
    if(consultas.fim < CONSULTA_MAX)
    {        
        int count, i, d, m, a, ind = 0;
        struct tm data;
        int test = 1;
        int idM, idP;
        printf("---- Cadastrar consulta ----\n");
        
        printf("Digite a data:  \n");
        printf("Dia: ");
        scanf(" %d", &d);
        printf("Mês (2 numeros): ");
        scanf(" %d", &m);
        printf("Ano (4 numeros): ");        
        scanf(" %d", &a);                     
           
        // Organiza na struct tm        
        data.tm_mday = d;
        data.tm_mon = m;
        data.tm_year = a;
        
        ind = mktime(&data);
        
        // Vê se precisa colocar em ordem na fila de consultas
        i = 0;
        if (consultas.fim > 0)
        {            
            for(i = 0; i < consultas.fim; i++)
            {                
                if (ind < consultas.consulta[i].dataConsulta)
                {
                    break;
                }
            }      
                        
            //Encontrou o lugar, empurrar os outros para frente
            for (int j = consultas.fim; j > i; j--)
            {
                consultas.consulta[j].dataConsulta = consultas.consulta[j-1].dataConsulta;
                consultas.consulta[j].crmMedico = consultas.consulta[j-1].crmMedico;
                consultas.consulta[j].idPaciente = consultas.consulta[j-1].idPaciente;
                strcpy(consultas.consulta[j].problema, consultas.consulta[j-1].problema);
                consultas.consulta[j].dia = consultas.consulta[j-1].dia;
                consultas.consulta[j].mes = consultas.consulta[j-1].mes;
                consultas.consulta[j].ano = consultas.consulta[j-1].ano;
            }
            
        }
        // Cadastra data simplificada para consulta
        consultas.consulta[i].dia = d;
        consultas.consulta[i].mes = m;
        consultas.consulta[i].ano = a;
        
        //Cadastra a data desse registro        
        consultas.consulta[i].dataConsulta = mktime(&data);        
                
        // Escolher medico
        printf("CRM do médico: ");
        scanf("%d", &consultas.consulta[i].crmMedico);       
        
        for(count = 0; count < medicos.fim; count++)
        {
            if (consultas.consulta[i].crmMedico == medicos.medico[count].crm)
            {
                test = 0;
                idM = count;
                break;
            }
        }
        
        if (test == 1)
        {
            printf("Médico não encontrado!! \nCancelando...\n");
            pause();
            return 0;
        }
        
        test = 1;
        
         // Escolher Paciente
        printf("Id do paciente: ");
        scanf("%d", &consultas.consulta[i].idPaciente);   
        
        for(count = 0; count < countPaciente; count++)
        {
            if (consultas.consulta[i].idPaciente == pacientes[count].id)
            {
                test = 0;
                idP = count;
                break;
            }
        }
        
        if (test == 1)
        {
            printf("Paciente não encontrado!! \nCancelando...\n");
            pause();
            return 0;
        }    
        
        test = 1;
        // Problema do desgraçado 
        printf("Problema do paciente: ");
        scanf("%s", consultas.consulta[i].problema);   
        fflush(stdin);
        
        printf("Consulta cadastrada com sucesso!\n");
        
        printf("Data:      %d %d %d\n", consultas.consulta[i].dia, consultas.consulta[i].mes, consultas.consulta[i].ano);
        printf("Médico:    %s\n", medicos.medico[idM].nome);
        printf("Paciente:  %s\n", pacientes[idP].nome);
        printf("Problema:  %s\n", consultas.consulta[i].problema);
        
        count = 0, i = 0;
        consultas.fim++;
        pause();                               
    }    
    else
    {
        printf("Máximo de consultas alcançado.\n");
        pause();
        
    }
}
void pesquisarConsulta()
{
    printf("------ Pesquisar Consulta ------ \n");
    if(consultas.fim > 0)
    {
        int i, ind, d, m, a, pacient,  idP, idM = 0;
        struct tm data;
        int flag = 0;
        
        // pesquisar se existe o Paciente
        printf("Id do paciente: ");
        scanf("%d", &pacient);   
        
        for(int count = 0; count < countPaciente; count++)
        {
            if (pacientes[count].id == pacient)
            {
                flag = 1;
                idP = count;
                break;
            }
        }        
        if (flag == 0)
        {
            printf("Paciente não cadastrado!\n");
            pause();
            return 0;
        }
        flag = 0;
        
        printf("Digite a data: \n");
        printf("Dia: ");
        scanf(" %d", &d);
        printf("Mês (2 numeros): ");
        scanf(" %d", &m);
        printf("Ano (4 numeros): ");        
        scanf(" %d", &a);                     
           
        // Organiza na struct tm        
        data.tm_mday = d;
        data.tm_mon = m;
        data.tm_year = a;
        
        ind = mktime(&data);
        
        for(i = 0; i < consultas.fim; i++)
        {                
            if (ind == consultas.consulta[i].dataConsulta && consultas.consulta[i].id == pacient)
            {                
                flag = 1;
                break;
                return 0;
            }
        }
        
        if (flag == 0)
        {
            printf("Não existe essa consulta!\n");
            pause();
            return 0;
            
        }        
        flag = 1;
        printf("Consulta: \n");
        
        printf("Data:      %d %d %d\n", consultas.consulta[i].dia, consultas.consulta[i].mes, consultas.consulta[i].ano);
        printf("Médico:    %s\n", medicos.medico[idM].nome);
        printf("Paciente:  %s\n", pacientes[idP].nome);
        printf("Problema:  %s\n", consultas.consulta[i].problema);
        i = 0;        
        pause();
    }
    else if(consultas.fim)
    {
        printf("Não há consultas cadastradas.\n");
        pause();
    }
    else
    {
        printf("Não há consultas\n");
        pause();
    }
}
void excluirConsulta()
{
    printf("------ Excluir Consulta ------ \n");
    if(consultas.fim < CONSULTA_MAX)
    {        
        int i, ind, index, d, m, a, pacient,  idP, idM = 0;
        struct tm data;
        int flag = 0;
        
        // pesquisar se existe o Paciente
        printf("Id do paciente: ");
        scanf("%d", &pacient);   
        
        for(int count = 0; count < countPaciente; count++)
        {
            if (pacientes[count].id == pacient)
            {
                flag = 1;
                idP = count;
                break;
            }
        }        
        if (flag == 0)
        {
            printf("Paciente não cadastrado!\n");
            pause();
            return 0;
        }
        flag = 0;
        
        printf("Digite a data: \n");
        printf("Dia: ");
        scanf(" %d", &d);
        printf("Mês (2 numeros): ");
        scanf(" %d", &m);
        printf("Ano (4 numeros): ");        
        scanf(" %d", &a);                     
           
        // Organiza na struct tm        
        data.tm_mday = d;
        data.tm_mon = m;
        data.tm_year = a;
        
        ind = mktime(&data);
        
        for(i = 0; i < consultas.fim; i++)
        {                
            if (ind == consultas.consulta[i].dataConsulta && consultas.consulta[i].id == pacient)
            {                
                flag = 1;
                index = i;
                break;                
            }
        }
        
        if (flag == 0)
        {
            printf("Não existe essa consulta!\n");
            pause();
            return 0;
            
        } 
        
              
        //se achou a consulta, reescreve os outros na frente
                   
        //Encontrou o lugar, empurrar os outros para frente
        for (int j = consultas.fim; j < index; j++)
        {
            consultas.consulta[j].dataConsulta = consultas.consulta[j+1].dataConsulta;
            consultas.consulta[j].crmMedico = consultas.consulta[j+1].crmMedico;
            consultas.consulta[j].idPaciente = consultas.consulta[j+1].idPaciente;
            strcpy(consultas.consulta[j].problema, consultas.consulta[j+1].problema);
            consultas.consulta[j].dia = consultas.consulta[j+1].dia;
            consultas.consulta[j].mes = consultas.consulta[j+1].mes;
            consultas.consulta[j].ano = consultas.consulta[j+1].ano;
        }          
        
        // Cadastra data simplificada para consulta
        consultas.consulta[i].dia = d;
        consultas.consulta[i].mes = m;
        consultas.consulta[i].ano = a;
        
        //Cadastra a data desse registro        
        consultas.consulta[i].dataConsulta = mktime(&data);        
                
        // Escolher medico
        printf("Consulta excluida\n");
        
        consultas.fim--;
        pause();                               
    }    
    else
    {
        printf("Não há consultas\n");
        pause();
    }
}
void alterarConsulta()
{
    printf("------ Alterar Consulta ------ \n");
    if(consultas.fim > 0)
    {
        int i, ind, d, m, a, pacient, count, test, idP, idM = 0;
        struct tm data;
        int flag = 0;
        
        // pesquisar se existe o Paciente
        printf("Id do paciente: ");
        scanf("%d", &pacient);   
        
        for(int count = 0; count < countPaciente; count++)
        {
            if (pacientes[count].id == pacient)
            {
                flag = 1;
                idP = count;
                break;
            }
        }        
        if (flag == 0)
        {
            printf("Paciente não cadastrado!\n");
            pause();
            return 0;
        }
        flag = 0;
        
        printf("Digite a data: \n");
        printf("Dia: ");
        scanf(" %d", &d);
        printf("Mês (2 numeros): ");
        scanf(" %d", &m);
        printf("Ano (4 numeros): ");        
        scanf(" %d", &a);                     
           
        // Organiza na struct tm        
        data.tm_mday = d;
        data.tm_mon = m;
        data.tm_year = a;
        
        ind = mktime(&data);
        
        for(i = 0; i < consultas.fim; i++)
        {                
            if (ind == consultas.consulta[i].dataConsulta && consultas.consulta[i].id == pacient)
            {                
                flag = 1;
                break;
                return 0;
            }
        }
        
        if (flag == 0)
        {
            printf("Não existe essa consulta!\n");
            pause();
            return 0;
            
        }        
        flag = 1;
        
        printf("Digite os novos dados da Consulta: \n");
        // Escolher medico
        printf("CRM do médico: ");
        scanf("%d", &consultas.consulta[i].crmMedico);       
        
        for(int count = 0; count < medicos.fim; count++)
        {
            if (consultas.consulta[i].crmMedico == medicos.medico[count].crm)
            {
                test = 0;
                idM = count;
                break;
            }
        }
        
        if (test == 1)
        {
            printf("Médico não encontrado!! \nCancelando...\n");
            pause();
            return 0;
        }
        
        printf("Problema do paciente: ");
        scanf("%s", consultas.consulta[i].problema);   
        fflush(stdin);
        
        printf("Consulta cadastrada com sucesso!\n");
        
        printf("Data:      %d %d %d\n", consultas.consulta[i].dia, consultas.consulta[i].mes, consultas.consulta[i].ano);
        printf("Médico:    %s\n", medicos.medico[idM].nome);
        printf("Paciente:  %s\n", pacientes[idP].nome);
        printf("Problema:  %s\n", consultas.consulta[i].problema);
        
        count = 0, i = 0;
        consultas.fim++;
        pause();                               
    }
    else
    {
        printf("Não há consultas\n");
        pause();
    }
}
void listaConsultaPorMedico()
{
    if(consultas.fim > 0 && medicos.fim > 0)
    {
        int crm, test, idM = 0;
        int flag = 0;
        printf("------ Listar Consulta ------ \n");
        
        // Escolher medico
        printf("CRM do médico: ");
        scanf("%d", &crm);       
        
        for(int count = 0; count < medicos.fim; count++)
        {
            if (crm == medicos.medico[count].crm)
            {
                flag = 1;
                idM = count;
                break;
            }
        }
        
        if (flag == 0)
        {
            printf("Médico não encontrado!! \nCancelando...\n");
            pause();
            return 0;
        }
        
        for(int i = 0; i < consultas.fim; i++)
        {                
            if (crm == consultas.consulta[i].crmMedico)
            {   
                printf("id da consulta:    %d\n", consultas.consulta[i].dataConsulta);
                printf("Data:      %d %d %d\n", consultas.consulta[i].dia, consultas.consulta[i].mes, consultas.consulta[i].ano);
                printf("Médico:    %s\n", medicos.medico[idM].nome);
                // not today printf("Paciente:  %s\n", pacientes[idP].nome);
                printf("Problema:  %s\n\n", consultas.consulta[i].problema);
                
            }
        }
        
        if (flag == 0)
        {
            printf("Não existe essa consulta!\n");
            pause();
            return 0;
            
        }        
        pause();
        
    }
    else
    {
        printf("Não há consultas\n");
        pause();
    }
}


int main(int argc, char** argv) {
    
    //Inicialização com Dados
    medicos.fim = 0;
    consultas.fim = 0;
    pacientes[1].id = 1;
    strcpy(pacientes[1].nome, "Joao");
    strcpy(pacientes[1].endereco, "Rua Av");
    pacientes[1].plano = 1;
    
    pacientes[2].id = 2;
    strcpy(pacientes[2].nome, "Joana");
    strcpy(pacientes[2].endereco, "Rua central do centro");
    pacientes[2].plano = 3;
    
    countPaciente = 3;
    
    medicos.medico[0].crm = 12345;
    strcpy(medicos.medico[0].nome, "Marco");
    strcpy(medicos.medico[0].especialidade, "Geral");
    
    medicos.medico[1].crm = 33345;
    strcpy(medicos.medico[1].nome,  "Maria");
    strcpy(medicos.medico[1].especialidade, "Dentista");
    
 
    medicos.fim = 2;
    
    
    consultas.consulta[0].dataConsulta = 1317147148;
    consultas.consulta[0].dia = 12;
    consultas.consulta[0].mes = 12;
    consultas.consulta[0].ano= 2016;
    consultas.consulta[0].crmMedico = 12345;
    consultas.consulta[0].idPaciente = 1;
    strcpy(consultas.consulta[0].problema, "O paciente afirma que 'Aaaaaah'");
    
    consultas.consulta[1].dataConsulta = 1325257148;
    consultas.consulta[1].dia = 25;
    consultas.consulta[1].mes = 12;
    consultas.consulta[1].ano = 2016;
    consultas.consulta[1].crmMedico = 33345;
    consultas.consulta[1].idPaciente = 2;
    strcpy(consultas.consulta[1].problema, "É Lúpus");
    
    consultas.fim = 2;
    
    menu();       
    return (EXIT_SUCCESS);
}

